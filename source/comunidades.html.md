---
title: Comunidades
layout: page
---

**Nacional**

- [Rails-br](https://groups.google.com/forum/#!forum/rails-br)

**Minas Gerais**

- [GURU-MG](https://groups.google.com/forum/#!forum/guru-mg)

**Pernambuco**

- [Frevo on rails](https://www.facebook.com/groups/frevoonrails/about/)

**Santa Catarina**

- [GURU-SC](https://groups.google.com/forum/#!forum/guru-sc)

**São Paulo**

- [Ruby-SP](https://groups.google.com/forum/#!forum/ruby-sp)

**Rio de Janeiro**

- [Ruby on rio](https://groups.google.com/forum/#!forum/rubyonrio)

**Rio Grande do Sul**

- [GURU-RS](https://www.facebook.com/groups/321605004647992/about/)

**Alagoas**

- [GURU-AL](https://groups.google.com/forum/#!forum/guru-al)

**Goiás**

- [Rails-GO](https://groups.google.com/forum/#!forum/rails-go)

**Ceará**

- [GURU-CE](https://groups.google.com/forum/#!forum/guru-ce)

**Centro-Oeste**

- [Ruby on cerrado](https://www.facebook.com/groups/rubyoncerrado/)

-----

Você conhece outra comunidade ou forum sobre ruby? Faça um [fork](https://gitlab.com/rubyonrailsbrasil/rubyonrailsbrasil.com.br) e adicione na lista.
