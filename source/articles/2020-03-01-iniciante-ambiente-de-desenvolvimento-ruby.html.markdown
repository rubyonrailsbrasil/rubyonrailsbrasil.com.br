---

title: "Ambiente de Desenvolvimento: Instalando o Ruby"
date: 2020-03-01 23:22 UTC
tags: Iniciante
layout: post
author: brodock
---

Uma dúvida comum de quem está iniciando no mundo Ruby é como montar seu ambiente de desenvolvimento, 
pra conseguir começar a testar tutoriais ou rodar aplicações open-source mais simples na sua máquina local. 

Neste artigo vou explicar como instalar mais de uma versão do Ruby e como rodar elas.

## Sistema operacional

Embora muita gente utilize Windows em seu computador como sistema operacional principal, quando estamos falando
sobre programação com Ruby, existem apenas duas soluções viáveis: Linux ou MacOS.

Pra quem tem acesso a um computador que rode MacOS, as instruções a seguir podem precisar de um ajuste ou detalhe
extra (eu vou avisar).

Vamos utilizar nos exemplos a distribuição Ubuntu, porque é a mais popular. Se você está em dúvida sobre qual
distribuição Linux escolher, considere que a maioria dos exemplos na internet estão escritos para Ubuntu,
portanto, a menos que você tenha alguém para te mentorar em alguma distribuição diferente, não vale a pena.

Se você por acaso não tem Linux instalado na sua máquina ou não quer instalar ele utilizando dual-boot, uma 
alternativa é rodar a linha de comando usando o suporte de [subsistema nativo Linux do Windows 10][WSL].

Instalando Linux via WSL, vai te dar suporte pra rodar a maioria das aplicações que dependem apenas de terminal.
Alguns casos mais específicos podem não funcionar corretamente, como rodar testes que controlem automaticamente
um navegador, ou algum recurso que dependa de um ambiente gráfico. O ideal é mesmo rodar Linux como ambiente
principal, mas isso pode ajudar você a se familiarizar antes de fazer a troca definitiva.

### Instalando Ubuntu via WSL no Windows 10

Antes de começar, você deve se certificar que seu sistema está completamente atualizado.

Para habilitar o WSL (suporte para Linux do Windows), você precisa usar o PowerShell em modo administrador:

1. No menu iniciar, digite `powershell` na pesquisa
1. Ao aparecer "Windows PowerShell", clique com botão direito e escolha "Executar como administrador"

Nesta tela que abrir, você vai digitar o seguinte:

```powershell
Enable-WindowsOptionalFeature -Online -FeatureName Microsoft-Windows-Subsystem-Linux
```

Reinicie seu computador.

Abra o aplicativo da Loja do Windows, e procure por Ubuntu. Basta clicar para instalar e pronto.

Ao final desse processo você vai ter um ícone no menu iniciar com nome "Ubuntu". Ao clicar nele você estará
carregando uma versão completamente funcional do Linux, com acesso ao terminal.

Na primeira vez que você abrir ele, ele pode pedir algumas informações para configurar contas de acesso por exemplo.

Não faz parte deste tutorial te ensinar o básico do Linux.

## Instalando o Ruby

Em uma distribuição Linux, existe suporte a pelo menos uma versão das linguagens de programação mais populares. 
Ela pode vir instalada automaticamente ou necessitar uma instalação via gerenciador de pacotes.

Embora seja possível utilizar essa versão oficial, pode ser que ela seja ou muito nova ou mais antiga, em relação
ao que você quer rodar. Se você está seguindo um livro ou um tutorial mais antigo, e quiser ter certeza que está
rodando na mesma versão, você vai precisar usar um gerenciador de versão.

Os dois principais gerenciadores de versão para Ruby são: [RVM] e [rbenv]. Se você perguntar na comunidade, vai
encontrar pessoas defendendo os dois. Ambos fazem a mesma coisa: te permitem trocar entre versões do Ruby
a qualquer momento. Seja especificando elas na pasta dos projetos ou utilizando uma configuração global pro teu
sistema operacional. O detalhe está em como elas fazem isso.

O **rbenv** te permite trocar entre versões do Ruby criando pequenos scripts no lugar dos executáveis do Ruby 
ou executáveis instalados pelas rubygems. Esses arquivos são chamados de [shims] e o que eles fazem é redirecionar
a execução para o rbenv (que vai então decidir qual versão usar).

Esta técnica necessita apenas adicionar algumas coisas no `PATH`, portanto é bem simples e segura, no entanto,
os shims adicionam um custo extra pra iniciar os comandos.

O **RVM** funciona de outra forma, ele carrega o código diretamente na shell. Com isso ele modifica o `PATH`
e demais variáveis de ambientes usadas pelo Ruby, rubygems e bundler pra trocar entre versões do Ruby. Com ele
é possível também criar **gemsets** (para instalar gems em ambientes isolados dos demais).

Neste artigo vamos focar no RVM, pela facilidade de instalar ele no Ubuntu.

### Instalando RVM no Ubuntu

Vamos instalar ele usando um pacote nativo do sistema operacional, que vai instalar além do RVM dependências que
geralmente são necessárias para compilar gems nativas para o sistema operacional.

Abra o seu terminal e digite o seguinte:

```bash
# Instala suporte para adicionar novos repositórios PPA:
sudo apt install software-properties-common

# Adiciona um novo repositório que contém os pacotes do RVM:
sudo apt-add-repository -y ppa:rael-gc/rvm

# Instala o pacote do RVM
sudo apt install rvm

# Habilitar o RVM na shell do usuário atual:
echo "source /etc/profile.d/rvm.sh" >> ~/.bashrc
```

Por último, precisamos adicionar o seu usuário ao grupo do `rvm` para que ele possa instalar novas versões do Ruby:  

```bash
# Adiciona o usuário atual ao grupo `rvm`
addgroup $(whoami) rvm

# Supondo que seu usuário seja `ubuntu` o comando acima é equivalente a digitar: 
# addgroup ubuntu rvm
```

Ao final deste processo, basta fechar o terminal e abrir novamente que o RVM vai estar funcionando.

Na seção [Usando o RVM](#usando-o-rvm), vamos ver como instalar diversas versões do Ruby.

### Instalando no MacOS

No MacOS, vamos precisar instalar primeiro o [HomeBrew], que é um gerenciador de pacotes suportado pela comunidade:

```bash
/usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
```

Em seguida vamos instalar algumas dependências:

```bash
# Instala o Xcode Command Tools
xcode-select --install

# Instala GNUPG (dependencia da instalação do RVM)
brew install gnupg

# Importa as chaves públicas do RVM para instalar/atualizar de forma segura
curl -sSL https://rvm.io/mpapis.asc | gpg --import -
curl -sSL https://rvm.io/pkuczynski.asc | gpg --import -
```

E por fim, instalar o RVM:

```bash
\curl -L https://get.rvm.io | bash -s stable
```

## Usando o RVM
Com o RVM instalado, você pode instalar e trocar entre múltiplas versões do Ruby usando os comandos a seguir:

```bash
# Instalar uma versão específica do ruby:
rvm install 2.6.5 # instala versão 2.6.5

# Trocar para uma versão específica:
rvm use 2.6.5 # passa a usar a versão 2.6.5

# Definir uma versão como a padrão:
rvm use 2.6.5 --default # define 2.6.5 como a padrão

# Remover uma versão: 
rvm remove 2.3.1 # desinstala completamente a versão 2.3.1

# Listar versões instaladas:
rvm list

# Listar as versões instaláveis:
rvm list known rubies
```

Outra forma de trocar entre uma versão ou outra, é criar um arquivo `.ruby-version` e colocar na pasta raiz do projeto.

Esse arquivo deve conter na primeira linha o código da versão e nada mais. Exemplo:

```bash
2.6.5
```

Tanto o RVM quando rbenv conseguem ler este arquivo e vão trocar automaticamente para esta versão, 
se ela estiver instalada. Este arquivo está presente em muitos projetos open-source e facilita muito
saber qual versão é suportada pelo projeto.

Ao tentar usar um projeto cuja versão ainda não está instalada, você receberá uma mensagem de erro avisando,
e neste caso, o RVM usará a versão padrão até você decidir instalar a correta.

---

Espero que este artigo ajude a remover a primeira barreira para se tornar um Programador Ruby. 

No próximo artigo, vou falar como executar os bancos de dados mais populares e alguns outros serviços que geralmente são usados
em aplicações Ruby on Rails. 



[WSL]: https://docs.microsoft.com/pt-br/windows/wsl/install-win10
[RVM]: https://rvm.io/rvm/install
[rbenv]: https://github.com/rbenv/rbenv#installation
[shims]: https://github.com/rbenv/rbenv#understanding-shims
[HomeBrew]: https://brew.sh
