# Comunidade Ruby on Rails Brasil

Nosso site é gerado usando o [Middleman](https://middlemanapp.com/) e hospedado
pelo [GitLab Pages](http://doc.gitlab.com/ee/pages).

A geração do HTML estático é feita automaticamente pelo Gitlab CI (veja `.gitlab-ci.yml`), e publicado
no GitLab pages.

## Contribuir

Este site está aberto a contribuições da comunidade. Sugerimos que leia a [documentação
do Middleman](https://middlemanapp.com/basics/install/) antes de enviar um Merge Request.

Alguns passos simples para começar:

```bash
git clone https://gitlab.com/rubyonrailsbrasil/rubyonrailsbrasil.com.br.git
bundle install

# inicia um servidor local para pré-visualizar modificações:
bundle exec middleman
```

### Enviar um novo artigo

Os artigos são todos escritos em Markdown. Siga os passos a baixo para gerar as informações necessárias.

1. Faça um fork do projeto e inicie um novo branch
1. Crie um arquivo baseado no template em markdown: 
   ```bash
   bundle exec middleman article "Titulo do artigo"
   ```
1. Adicione seus dados em `data/authors.yml`
1. Edite o frontmatter do artigo para linkar a autoria: `author: seu_usuario` (conforme adicionado no `data/authors.yml`)
1. Utilize uma ou mais tags baseado nas existentes
1. Faça o commit e envie um Merge Request para o projeto principal
