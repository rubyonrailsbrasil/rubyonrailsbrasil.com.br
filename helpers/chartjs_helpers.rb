# frozen_string_literal: true

# Enable use of ChartJS in page content
module ChartjsHelpers
  # @attribute type: Type of chart to render ('line' or 'bar')
  # @attribute focus: Which tab should be initially focused ('chart' or 'table')
  # @attribute series: Is each row a new series in the data, or is each column? ('columns' or 'rows')
  # @attribute data: Any other data attributes passed to Chart.js directly
  def chart_placeholder(type: 'line', focus: 'chart', series: 'columns', data:)
    data_attrs = { figure: 'chart', type: type, focus: focus, series: series }
    data_attrs.merge(data) if data
    haml_tag(:div, tag, data: data_attrs)
  end
end
